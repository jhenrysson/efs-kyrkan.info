<?php get_header(); ?>

<main class="row">
    <div class="col-12 p-0">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="text-center">
                <h1 class="d-inline-block single-title"><?php the_title(); ?></h1>
            </div>

            <?php if (has_post_thumbnail()) : ?>
                <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-fluid mx-auto d-block thumbnail">
            <?php endif; ?>

            <div class="col-sm-10 offset-sm-1 col-lg-8 offset-lg-2">
                <?php the_content(); ?>
            </div>
        <?php endwhile; else : ?>
            <h1><?php _e('Not found'); ?></h1>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>
    </div>
</main>

<?php get_footer(); ?>
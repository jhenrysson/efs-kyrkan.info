<?php

require_once __DIR__ . '/classes/EfsStyle.php';
require_once __DIR__ . '/classes/EfsJavascript.php';
require_once __DIR__ . '/classes/EfsMeta.php';
require_once __DIR__ . '/classes/EfsMenu.php';
require_once __DIR__ . '/classes/EfsThemeSupport.php';
require_once __DIR__ . '/classes/EfsEmployee.php';
require_once __DIR__ . '/classes/EfsBoard.php';
require_once __DIR__ . '/classes/EfsFrontpagePuffs.php';

require_once __DIR__ . '/classes/EfsFrontpageTv.php';
require_once __DIR__ . '/classes/EfsCalendar.php';

require_once __DIR__ . '/classes/EfsMenuBootstrapWalker.php';

add_action('wp_enqueue_scripts', ['EfsStyle', 'addCss']);
add_action('wp_enqueue_scripts', ['EfsJavascript', 'addJs']);
add_action('wp_head', ['EfsMeta', 'addMeta']);
add_action('wp_head', function() {
    require_once __DIR__ . '/tracking/google-analytics.html';
});
add_action('init', ['EfsMenu', 'addMenus']);
add_action('init', ['EfsEmployee', 'registerPostType']);
add_action('init', ['EfsBoard', 'registerPostType']);
add_action('init', ['EfsFrontpagePuffs', 'registerPostType']);
add_action('after_setup_theme', ['EfsThemeSupport', 'addFeatures']);
add_action('after_setup_theme', ['EfsFrontpagePuffs', 'initThumbnailSize']);

add_filter('tribe_events_list_the_date_headers', ['EfsCalendar', 'dateHeaderFilter'], 10, 3);
add_filter('tribe_get_start_date', ['EfsCalendar', 'startDateFilter']);

add_filter('body_class', function(array $classes): array {
    if (get_page_template_slug() != 'page-templates/static-front-page.php' && !is_home()) {
        return $classes;
    }

    if (is_home()) {
        $classes[] = 'corona-home';
        return $classes;
    }

    $classes[] = 'home';
    return $classes;
});

<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @version 4.6.19
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<div class="col-4 col-md-3 col-lg-2 date">
    <span class="day"><?php print tribe_get_start_date(null, false, 'd'); ?></span>
    <span class="month"><?php print tribe_get_start_date(null, false, 'F'); ?></span>
</div>

<div class="col-8 col-md-9 col-lg-10">
    <h3><a href="<?php print esc_url(tribe_get_event_link()); ?>"><?php the_title(); ?></a></h3>
    <span class="time"><?php print tribe_events_event_schedule_details(); ?></span> <?php
        $address_delimiter = empty( $venue_address ) ? ' ' : ', ';

        // These details are already escaped in various ways earlier in the process.
        echo implode( $address_delimiter, $venue_details );

        if ( tribe_show_google_map_link() ) : ?>
        <span class="location"><?php print tribe_get_map_link_html(); ?></span>
    <?php endif; ?>
    <p>
        <?php print tribe_events_get_the_excerpt(); ?>
    </p>
</div>

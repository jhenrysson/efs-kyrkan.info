<?php
/**
 * List View Title Template
 * The title template for the list view of events.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/title-bar.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 * @since   4.6.19
 *
 */
?>

<div class="row">
    <div class="col-12 text-center">
        <!-- List Title -->
        <?php do_action( 'tribe_events_before_the_title' ); ?>
        <h1 class="d-inline-block">Kalender</h1>
        <?php do_action( 'tribe_events_after_the_title' ); ?>
    </div>

    <div class="col-12 text-center banner p-0">
        <img src="<?php print get_stylesheet_directory_uri(); ?>/images/calendar/hero.jpg?v2" alt="Kalender banner" class="img-fluid">
    </div>
</div>

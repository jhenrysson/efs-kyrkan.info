<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div class="container-fluid">

        <header class="row">
            <div class="col-md-2 col-sm-4 col-6">
                <a href="<?php print get_home_url(); ?>">
                    <img src="<?php print get_stylesheet_directory_uri(); ?>/images/logotype-red.svg" class="img-fluid">
                </a>
            </div>

            <div class="col-md-8 d-none d-md-block">
                <?php wp_nav_menu([
                    'theme_location' => 'top-menu',
                    'fallback_cb' => false,
                    'depth' => 1,
                    'container' => 'nav',
                    'menu_class' => 'nav justify-content-center top-menu',
                    'walker' => new EfsMenuBootstrapWalker()
                ]); ?>
            </div>

            <div class="col-md-2 col-sm-8 col-6 text-right">
                <a class="hamburger-menu-toggle">
                    <img src="<?php print get_stylesheet_directory_uri(); ?>/images/hamburger-menu.svg" class="img-fluid" alt="Meny">
                </a>
            </div>
            <?php wp_nav_menu([
                'theme_location' => 'hamburger-menu',
                'fallback_cb' => false,
                'container' => 'nav',
                'container_class' => 'hamburger-menu col-12',
            ]); ?>
        </header>
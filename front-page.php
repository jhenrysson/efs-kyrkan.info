<?php
/**
 * Template Name: Static front page template
 */
?>

<?php get_header(); ?>

    <div class="row top-banner justify-content-center" style="background: url('<?php print get_header_image(); ?>') center/cover no-repeat;">
        <div class="col-10 col-sm-8 col-md-6 col-lg-5 col-xl-4 text-center">
            <h1 class="d-inline-block">Välkommen hem</h1>
            <p>
                Vi är en kyrka som består av människor av alla de slag. Gud är det viktigaste i våra liv, och av Honom
                vill vi dela med oss till andra. Vi bygger vår kyrka på gemenskap, glädje och kärlek.
            </p>
            <p>
                Välkommen in!
            </p>
        </div>
        <div class="col-12 text-center social-media-links">
            <a href="https://www.instagram.com/efskyrkanahlm/" title="Se EFS-kyrkan i Ängelholm på Instagram!" target="_blank">
                <img src="<?php print get_stylesheet_directory_uri(); ?>/images/front-page/logotype-instagram.svg" alt="Instagram">
            </a>
            <a href="https://www.facebook.com/efskyrkan/" title="Se EFS-kyrkan i Ängelholm på Facebook!" target="_blank">
                <img src="<?php print get_stylesheet_directory_uri(); ?>/images/front-page/logotype-facebook.svg" alt="Facebook">
            </a>
        </div>
    </div>

    <div class="row about-us justify-content-center">
        <div class="col-12 col-md-5 col-lg-4">
            <h1 class="d-inline-block">Det här är vår kyrka</h1>
            <blockquote>
                "En kyrka som återspeglar Guds hjärta bär varandra och betjänar människor, varhelst vi befinner oss,
                varje dag."
            </blockquote>
            <p>
                Så lyder EFS-Kyrkan i Ängelholms vision och den vill vi utforma vår verksamhet ifrån. Med ett tydligt
                fokus på Jesus och att få bli mer lika honom tror vi att vi kan få leva vår vision inte bara på
                söndagar utan även i vardagen genom att vara Guds folk.
            </p>
            <p>
                <a href="/det-har-ar-var-kyrka/" title="Läs mer om EFS-kyrkan i Ängelholm" class="read-more float-right">Läs mer</a>
            </p>
        </div>

        <div class="col-12 col-md-5 col-lg-4 offset-md-1 embed-responsive embed-responsive-16by9">
            <iframe src="//player.vimeo.com/video/158628798?portrait=0" width="900" height="500" frameborder="0" title="EFS-Kyrkan i Ängelholm Visionsfilm Kort" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>

    <?php if (EfsFrontpagePuffs::isTherePuffs()) : ?>
        <div class="row upcoming justify-content-center text-center">
            <div class="col-12 header">
                <h1 class="d-inline-block">På gång</h1>
                <p>Se vad som är på gång i kyrkan</p>
            </div>

            <?php foreach (EfsFrontpagePuffs::getPuffs() as $puff) : ?>
                <div class="col-12 col-sm-6 col-lg-3 item">
                    <a href="<?php print get_post_meta($puff->ID, EfsFrontpagePuffs::CUSTOM_URL, true); ?>"><img src="<?php print get_the_post_thumbnail_url($puff, EfsFrontpagePuffs::KEY); ?>" alt="<?php print get_the_title($puff); ?>" class="img-fluid">
                    <h2><?php print get_the_title($puff); ?></h2>
                        <p><?php print get_the_excerpt($puff); ?></p></a>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <div class="row calendar">
        <div class="d-none d-sm-block col-sm-3 col-md-5 col-lg-6 calendar-image"></div>

        <div class="col-12 col-sm-9 col-md-7 col-lg-6 information">
            <h1 class="d-inline-block">Kalender</h1>
            <p>
                Här kan du hålla koll på allt som är på gång i EFS-Kyrkan den närmaste tiden. Genom att klicka på
                aktiviteten får du information om tid och plats.
            </p>
            <?php foreach (EfsCalendar::getUpcomingEvents() as $event) : ?>
                <p>
                    <strong>
                        <a href="<?php print esc_url(tribe_get_event_link($event->ID)); ?>">
                            <?php print tribe_get_start_date($event->ID); ?> <?php print get_the_title($event); ?>
                        </a>
                    </strong>
                </p>
            <?php endforeach; ?>

            <p class="align-bottom">
                <a href="<?php print esc_url(tribe_get_events_link()); ?>" title="Läs mer om EFS-kyrkan i Ängelholm" class="read-more float-right">Hela kalendern</a>
            </p>
        </div>
    </div>

    <div class="row play text-center justify-content-center">
        <div class="col-12">
            <h1 class="d-inline-block">efs-kyrkan.tv</h1>
            <p>
                Vill du se söndagens predikan i efterhand eller lyssna till annan undervisning?<br>
                Välkommen till vår play-kanal efs-kyrkan.tv
            </p>
        </div>
        <div class="col-12 col-sm-10 col-md-8 col-lg-6 embed-responsive embed-responsive-16by9">
            <?php print EfsFrontpageTv::getLatestVideo()->video_embed; ?>
        </div>
        <div class="col-12">
            <p>
                <a href="http://efs-kyrkan.tv" title="Gå till efs-kyrkan.tv" target="_blank">Klicka här för att komma till efs-kyrkan.tv</a>
            </p>
        </div>
    </div>

<?php get_footer(); ?>
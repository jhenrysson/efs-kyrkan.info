# Use sass (scss)

## Installera sass

`sudo gem install sass`

## Watch sass development

`sass --watch scss:css-dev --style expanded`

## Update sass production

`sass --update scss:css --style compressed`

## Sass help

`sass --help`
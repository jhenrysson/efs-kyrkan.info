<?php

class EfsMeta
{
    public static function addMeta()
    {
        $object = new self();
        $object->addViewport();
    }

    /**
     * @return string
     */
    private function addViewport()
    {
        print '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
    }
}

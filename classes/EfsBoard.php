<?php

/**
 * Define efs board custom post type, to structure data and minimize the risk of anything going wrong.
 */
class EfsBoard
{
    const KEY = 'efs-board';

    public static function registerPostType()
    {
        $args = [
            'label' => 'Styrelsen',
            'labels' => [
                'name' => 'Styrelse',
                'singular_name' => 'Styrelsemedlem',
                'add_new' => 'Skapa ny',
                'add_new_item' => 'Skapa ny styrelsemedlem',
                'edit_item' => 'Ändra styrelsemedlem',
                'new_item' => 'Ny styrelsemedlem',
                'view_item' => 'Se styrelsemedlem',
                'view_items' => 'Se styrelsemedlemmar',
                'search_items' => 'Sök styrelsemedlemmar',
                'not_found' => 'Styrelsemedlem hittades inte',
                'not_found_trash' => 'Styrelsemedlem hittades inte i papperskorgen',
                'all_items' => 'Alla styrelsemedlemmar',
                'insert_into_item' => 'Lägg till i styrelsemedlem',
                'uploaded_to_this_item' => 'Uppladdad till styrelsemedlem',
                'featured_image' => 'Bild på styrelsemedlem',
                'set_featured_image' => 'Välj bild på styrelsemedlem',
                'remove_featured_image' => 'Ta bort bild på styrelsemedlem',
                'use_featured_image' => 'Använd bild för styrelsemedlem',
            ],
            'description' => 'Lista med styrelsemedlemmar',
            'rewrite' => ['slug' => 'styrelse'],
            'supports' => ['title', 'editor', 'thumbnail'],
            'public' => true,
            'has_archive' => true,
        ];

        register_post_type(self::KEY, $args);
    }
}

<?php

class EfsCalendar
{
    /**
     * Use filter for Tribe events calendar - to change date header in list view.
     *
     * @param string $html
     * @param int $eventMonth
     * @param int $eventYear
     * @return string
     */
    public static function dateHeaderFilter($html, $eventMonth, $eventYear)
    {
        if (empty($html)) {
            return $html;
        }

        $object = new self();
        $dateTime = $object->getHeaderDate($eventMonth, $eventYear);

        $container = '<div class="row"><div class="col-12 current-month"><h2>%s, %s</h2></div></div>';
        $html = sprintf($container, __($dateTime->format('F'), 'the-events-calendar'), $dateTime->format('Y'));

        return $html;
    }

    /**
     * Get upcoming events.
     *
     * @return array
     */
    public static function getUpcomingEvents()
    {
        $events = tribe_get_events([
            'posts_per_page' => 5,
            'start_date' => date('Y-m-d'),
        ]);

        // var_dump($events); die();
        return $events;
    }

    public static function startDateFilter($date)
    {
        // For now we only work with front-page.
        if (!is_front_page()) {
            return $date;
        }

        $date = explode(" ", $date);
        return sprintf(
            '<span class="date">%d %s</span><br>%s',
            $date[0],
            $date[1],
            $date[2]
        );
    }

    /**
     * Get tribe events calendar notices, wrapped in bootstrap grid system.
     *
     * @return string
     */
    public static function getNotices()
    {
        $notices = tribe_the_notices(false);

        if (empty($notices)) {
            return '';
        }

        return '<div class="row justify-content-center"><div class="col-8">' . $notices . '</div></div>';
    }

    /**
     * Get formatted string for header date.
     *
     * @param int $month
     * @param int $year
     * @return DateTime
     */
    private function getHeaderDate($month, $year)
    {
        $date = new DateTime();
        $date->setDate($year, $month, 1);

        return $date;
    }
}

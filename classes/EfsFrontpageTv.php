<?php

class EfsFrontpageTv
{
    const TRANSIENT = 'efs-frontpage-video';

    public static function getLatestVideo()
    {
        $object = new self();
        $data = $object->getData();

        return $data;
    }

    private function getData()
    {
        $data = get_transient(self::TRANSIENT);

        if ($data !== false) {
            return $data;
        }

        $data = $this->getDataFromRemote();

        // Keep in "cache" for 15 minutes
        if (!set_transient(self::TRANSIENT, $data, 900)) {
            trigger_error('Failed to set transient for frontpage tv.', E_USER_WARNING);
        }

        return $data;
    }

    /**
     * Get data from remote source.
     *
     * @todo Due to some weird issue json-decode doesn't work, this is a workaround for now.
     */
    private function getDataFromRemote()
    {
        $url = 'http://efs-kyrkan.tv/wp-content/themes/efs-kyrkan.tv/muppet.php?hash=2d1nub5TaD0t3kjOiK';
        $raw_data = wp_remote_get($url);

        $data = json_decode($raw_data['body']);
        if (!$data) {
            trigger_error('Json error: ' . json_last_error_msg(), E_USER_ERROR);
        }

        $data->video_embed = html_entity_decode($data->video_embed);

        return $data;
    }
}

<?php

/**
 * Define efs employee custom post type, to structure data and minimize the risk of anything going wrong.
 */
class EfsEmployee
{
    const KEY = 'efs-employee';

    public static function registerPostType()
    {
        $args = [
            'label' => 'Anställda',
            'labels' => [
                'name' => 'Anställda',
                'singular_name' => 'Anställd',
                'add_new' => 'Skapa ny',
                'add_new_item' => 'Skapa ny anställd',
                'edit_item' => 'Ändra anställd',
                'new_item' => 'Ny anställd',
                'view_item' => 'Se anställd',
                'view_items' => 'Se anställda',
                'search_items' => 'Sök anställda',
                'not_found' => 'Anställd hittades inte',
                'not_found_trash' => 'Anställd hittades inte i papperskorgen',
                'all_items' => 'Alla anställda',
                'insert_into_item' => 'Lägg till i anställd',
                'uploaded_to_this_item' => 'Uppladdad till anställd',
                'featured_image' => 'Bild på anställd',
                'set_featured_image' => 'Välj bild på anställd',
                'remove_featured_image' => 'Ta bort bild på anställd',
                'use_featured_image' => 'Använd bild för anställd',
            ],
            'description' => 'Lista med anställda',
            'rewrite' => ['slug' => 'medarbetare'],
            'supports' => ['title', 'editor', 'thumbnail'],
            'public' => true,
            'has_archive' => true,
        ];

        register_post_type(self::KEY, $args);
    }
}

<?php

class EfsJavascript
{
    public static function addJs()
    {
        $object = new self();
        $object->addHamburgerMenu();
    }

    private function addHamburgerMenu()
    {
        wp_enqueue_script('efs-hamburger-menu', get_stylesheet_directory_uri() . '/js/hamburger-menu.js', ['jquery'],
            EfsThemeSupport::VERSION, true);
    }
}

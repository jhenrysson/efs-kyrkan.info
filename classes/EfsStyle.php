<?php

class EfsStyle
{
    public static function addCss()
    {
        $object = new self();
        $object->addBootstrap();
        $object->addCustomCss();
    }

    /**
     * Load Bootstrap css. This is always production version.
     */
    private function addBootstrap()
    {
        wp_enqueue_style(
            'efs-bootstrap',
            get_stylesheet_directory_uri() . '/css/bootstrap/bootstrap.min.css',
            [],
            EfsThemeSupport::VERSION
        );
    }

    /**
     * Load custom css, from either css development folder or production.
     */
    private function addCustomCss()
    {
        wp_enqueue_style(
            'efs-custom-css',
            get_stylesheet_directory_uri() . '/' . $this->getCssFolder() . '/general.css',
            ['efs-bootstrap'],
            EfsThemeSupport::VERSION
        );
    }

    /**
     * Get css folder depending on site running as development or production.
     *
     * @return string
     */
    private function getCssFolder()
    {
        return WP_DEBUG ? 'css-dev' : 'css';
    }
}

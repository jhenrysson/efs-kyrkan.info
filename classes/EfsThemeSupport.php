<?php

class EfsThemeSupport
{
    const VERSION = '6.4.2';

    public static function addFeatures()
    {
        $object = new self();
        // @todo Fix custom logo some time in future - must set size etc first.
        // $object->addCustomLogo();
        $object->addHtml5Markup();
        $object->addTitleTag();
        $object->addPostThumbnail();
        $object->addCustomHeader();
    }

    private function addCustomLogo()
    {
        add_theme_support('custom-logo');
    }

    /**
     * @todo Add which html5 markup theme supports.
     */
    private function addHtml5Markup()
    {
        add_theme_support('html5');
    }

    private function addTitleTag()
    {
        add_theme_support('title-tag');
    }

    /**
     * @todo Config size of post thumbnail.
     */
    private function addPostThumbnail()
    {
        add_theme_support('post-thumbnails');
    }

    private function addCustomHeader()
    {
        $settings = [
            'default-image' => get_template_directory_uri() . '/images/front-page/top-banner-default.jpg',
            'width' => 1600,
            'height' => 800,
        ];

        add_theme_support('custom-header', $settings);
    }
}

<?php

class EfsMenu
{
    public static function addMenus()
    {
        $object = new self();
        $object->addTopMenu();
        $object->addHamburgerMenu();
    }

    private function addTopMenu()
    {
        register_nav_menu('top-menu', __('Menu at top'));
    }

    private function addHamburgerMenu()
    {
        register_nav_menu('hamburger-menu', __('Hamburger menu at top'));
    }
}

<?php

/**
 * Define efs employee custom post type, to structure data and minimize the risk of anything going wrong.
 */
class EfsFrontpagePuffs
{
    const KEY = 'efs-frontpage-puffs';
    const CUSTOM_URL = 'efs-frontpage-puff-url';

    private static $frontpagePuffs;

    public static function registerPostType()
    {
        $args = [
            'label' => 'Puffar på framsida',
            'labels' => [
                'name' => 'Puffar på framsida',
                'singular_name' => 'Puff på framsida',
                'add_new' => 'Skapa ny',
                'add_new_item' => 'Skapa ny puff',
                'edit_item' => 'Ändra puff',
                'new_item' => 'Ny puff',
                'view_item' => 'Se puff',
                'view_items' => 'Se puffar',
                'search_items' => 'Sök puffar',
                'not_found' => 'Puff hittades inte',
                'not_found_trash' => 'Puff hittades inte i papperskorgen',
                'all_items' => 'Alla puffar',
                'insert_into_item' => 'Lägg till i puff',
                'uploaded_to_this_item' => 'Uppladdad till puff',
                'featured_image' => 'Bild för puff',
                'set_featured_image' => 'Välj bild för puff',
                'remove_featured_image' => 'Ta bort bild från puff',
                'use_featured_image' => 'Använd bild för puff',
            ],
            'description' => 'Lista med puffar',
            'supports' => ['title', 'excerpt', 'custom-fields', 'thumbnail'],
            'show_ui' => true,
        ];

        register_post_type(self::KEY, $args);
    }

    /**
     * Check if there are any puffs, and store them in static if there are (to avoid multiple queries for same thing.)
     *
     * @return bool
     */
    public static function isTherePuffs()
    {
        self::$frontpagePuffs = get_posts([
           'numberposts' => 4,
            'post_type' => self::KEY,
        ]);

        return !empty(self::$frontpagePuffs);
    }

    /**
     * Get puffs if there are any.
     *
     * @return array
     */
    public static function getPuffs()
    {
        if (empty(self::$frontpagePuffs) && self::isTherePuffs()) {
            return [];
        }

        return self::$frontpagePuffs;
    }

    public static function initThumbnailSize()
    {
        add_image_size(self::KEY, 360, 185, true);
    }
}

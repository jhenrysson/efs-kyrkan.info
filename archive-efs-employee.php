<?php get_header(); ?>

    <main class="row col-lg-10 offset-lg-1">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="col-6 col-sm-4 col-md-3">
                <?php if (has_post_thumbnail()) : ?>
                    <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-fluid mx-auto d-block">
                <?php endif; ?>
                <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
            </div>
        <?php endwhile; else : ?>
            <div class="col-12">
                <h1><?php _e('Not found'); ?></h1>
                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            </div>
        <?php endif; ?>
    </main>

<?php get_footer(); ?>
<footer id="page-footer" class="row text-center">
    <div class="col-12">
        <h1>Kontakta EFS-kyrkan i Ängelholm</h1>
    </div>

    <div class="col-md-4">
        <img src="<?php print get_stylesheet_directory_uri(); ?>/images/phone.svg" alt="Phone">
        <h2>Telefon</h2>
        <p>
            <a href="tel:+46-431-12580">0431 - 125 80</a>
        </p>
    </div>

    <div class="col-md-4">
        <img src="<?php print get_stylesheet_directory_uri(); ?>/images/location.svg" alt="Location">
        <h2>Besöksadress</h2>
        <p>
            Metallgatan 31<br>
            262 72 Ängelholm
        </p>
    </div>

    <div class="col-md-4">
        <img src="<?php print get_stylesheet_directory_uri(); ?>/images/mail.svg" alt="Mail">
        <h2>E-post</h2>
        <p>
            <a href="mailto:info@efs-kyrkan.info">info@efs-kyrkan.info</a>
        </p>
    </div>

    <div class="col-12 text-center copyright">
        <p>
            EFS-Kyrkan i Ängelholm<br>
            &copy; Copyright 2002-<?php print date('Y'); ?>
        </p>
    </div>
</footer>

</div> <!-- .container-fluid -->
<?php wp_footer(); ?>
</body>
</html>